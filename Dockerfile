# ===============================================================
# base image

FROM debian:9.9


RUN \
  apt-get update -q && \
  apt-get install -yq \
  --no-install-recommends \
  git-core gcc g++ cmake pkg-config wget ca-certificates tar bzip2 \
  llvm-4.0-dev clang-4.0 lldb-4.0 lld-4.0 llvm-4.0-dev libclang-4.0-dev ninja-build \
  libgmp-dev libssl-dev libcurl4-openssl-dev libusb-1.0-0-dev zlib1g-dev libgmp-dev


RUN \
  update-alternatives --install /usr/bin/clang clang /usr/lib/llvm-4.0/bin/clang 400 && \
  update-alternatives --install /usr/bin/clang++ clang++ /usr/lib/llvm-4.0/bin/clang++ 400

# ===============================================================
# build boost 1.67.0

WORKDIR /root/
RUN wget https://dl.bintray.com/boostorg/release/1.67.0/source/boost_1_67_0.tar.bz2 -O - | tar -xj
WORKDIR /root/boost_1_67_0

RUN ./bootstrap.sh --prefix=/usr/local
RUN echo 'using clang : 4.0 : clang++-4.0 ;' >> project-config.jam
RUN \
  ./b2 -d0 -j$(nproc) \
  --with-thread \
  --with-date_time \
  --with-system \
  --with-filesystem \
  --with-program_options \
  --with-signals \
  --with-serialization \
  --with-chrono \
  --with-test \
  --with-context \
  --with-locale \
  --with-coroutine \
  --with-iostreams \
  toolset=clang \
  link=static \
  install


# ===============================================================
# arguments for build stage

ARG branch=master
ARG symbol=SYS


# ===============================================================
# build eosio

WORKDIR /root/
RUN git clone -b $branch https://github.com/EOSIO/eos.git --recursive
WORKDIR /root/eos

# patch cmake for packaging power
RUN echo 'include (CPack)' >> CMakeLists.txt
# purge line with chmod non-existent directory
RUN sed -i '/node_00/g' debian/postinst
# let's home to be created
RUN sed -i 's/ --no-create-home//g' debian/postinst


WORKDIR /root/eos/cmake-build
RUN cmake \
  -GNinja \
  -DENABLE_STATIC=ON \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/usr/ \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_CXX_COMPILER=clang++ \
  -DCMAKE_C_COMPILER=clang \
  -DBUILD_MONGO_DB_PLUGIN=false \
  -DBUILD_DOXYGEN=false \
  -DCORE_SYMBOL_NAME=$symbol \
  ..

# build
RUN \
  cmake --build "${PWD}"

# package
RUN \
  cpack -G DEB \
  -D CPACK_DEBIAN_PACKAGE_MAINTAINER="Eduard Generalov <eduard@generalov>"

# -D CPACK_PACKAGE_VERSION_MAJOR=1
# -D CPACK_PACKAGE_VERSION_MINOR=7
# -D CPACK_PACKAGE_VERSION_PATCH=4


# ===============================================================
# unarchive .deb and copy to clean system

RUN \
  dpkg -x EOS*.deb /tmp/eosio/

#  ar x *.deb && \
#  mkdir -p /tmp/eosio/ && \
#  tar xzvf data.tar.gz -C /tmp/eosio/

FROM debian:9.9-slim

RUN \
  apt-get update -q && \
  apt-get install -yq libcurl3 libssl1.1 libusb-1.0-0 --no-install-recommends && \
  apt-get clean

COPY --from=0 /tmp/eosio/ /
